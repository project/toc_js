/**
 * @file
 * Toc.js.
 */
(($, once) => {
  function decodeHtml(html) {
    const textarea = document.createElement('textarea');
    textarea.innerHTML = html;
    return textarea.value;
  }
  function convertIntToPx(value) {
    if (Number.isInteger(value)) {
      return `${value}px`;
    }
    if (typeof value === 'string') {
      const trimmedValue = value.trim();
      if (/^-?[0-9]+$/.test(trimmedValue)) {
        return `${trimmedValue}px`;
      }
    }
    return value;
  }
  function checkCss(property, value) {
    return CSS.supports(property, value) ? value : null;
  }
  function stringToArray(value, separator = /[,\s]+/, dflt = []) {
    if (typeof value === 'string' && value !== '') {
      // We split the string value using the provided separator.
      // If no separator is provided, comma and spaces are used as separators.
      // We finally filter out empty strings.
      return value.split(separator).filter((v) => v !== '');
    }
    return dflt;
  }
  Drupal.behaviors.toc_js = {
    attach(context) {
      once('toc_js', '.toc-js', context).forEach((elt) => {
        const $tocEl = $(elt);
        const options = {
          listType: $tocEl.data('list-type') === 'ol' ? 'ol' : 'ul',
          listClasses: stringToArray($tocEl.data('list-classes')),
          selectors: decodeHtml($tocEl.data('selectors')) || 'h2, h3',
          container: decodeHtml($tocEl.data('container')) || 'body',
          selectorsMinimum: $tocEl.data('selectors-minimum') || 0,
          sticky: !!$tocEl.data('sticky'),
          stickyOffset: checkCss(
            'top',
            convertIntToPx($tocEl.data('sticky-offset')),
          ),
          prefix: $tocEl.data('prefix') || '',
          liClasses: $tocEl.data('li-classes') || '',
          inheritableClasses: stringToArray($tocEl.data('inheritable-classes')),
          headingClasses: $tocEl.data('heading-classes') || '',
          skipInvisibleHeadings: !!$tocEl.data('skip-invisible-headings'),
          useHeadingHtml: !!$tocEl.data('use-heading-html'),
          collapsibleItems: elt.dataset.collapsibleItems === '1',
          collapsibleExpanded: elt.dataset.collapsibleExpanded === '1',
          backToTop: !!$tocEl.data('back-to-top'),
          backToTopLabel: $tocEl.data('back-to-top-label') || 'Back to top',
          backToTopClass: 'back-to-top',
          backToTopSelector: decodeHtml(elt.dataset.backToTopSelector) || '',
          headingFocus: !!$tocEl.data('heading-focus'),
          backToToc: !!$tocEl.data('back-to-toc'),
          backToTocLabel:
            $tocEl.data('back-to-toc-label') || 'Back to table of contents',
          backToTocClass: 'back-to-toc',
          backToTocClasses:
            $tocEl.data('back-to-toc-classes') || 'visually-hidden-focusable',
          highlightOnScroll: !!$tocEl.data('highlight-on-scroll'),
          highlightOffset: $tocEl.data('highlight-offset') || 0,
          smoothScrolling: !!$tocEl.data('smooth-scrolling'),
          scrollToOffset: checkCss(
            'scroll-margin-top',
            convertIntToPx($tocEl.data('scroll-to-offset')),
          ),
          tocContainer: decodeHtml($tocEl.data('toc-container')) || '',
          ajaxPageUpdates: !!$tocEl.data('ajax-page-updates'),
          observableSelector:
            decodeHtml($tocEl.data('observable-selector')) || '',
        };
        $tocEl.toc_js(options);
      });
    },
  };
})(jQuery, once);
